const { src, dest, watch, parallel, series } = require("gulp");

const scss = require("gulp-sass");
const concat = require("gulp-concat");
const browserSync = require("browser-sync").create();
const uglify = require("gulp-uglify-es").default;
const autoprefixer = require("gulp-autoprefixer");
const imagemin = require("gulp-imagemin");
const del = require("del");

function browsersync() {
    browserSync.init({
        server: {
            baseDir: "src/",
        },
    });
}

function cleanDist() {
    return del(["dist"]);
}

function cleanSrc() {
    return del(["src/css", "src/js/scripts.min.js", "src/js/vendor.min.js"]);
}

function images() {
    return src("src/images/**/*")
        .pipe(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.mozjpeg({ quality: 75, progressive: true }),
                imagemin.optipng({ optimizationLevel: 5 }),
                imagemin.svgo({
                    plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
                }),
            ])
        )
        .pipe(dest("dist/images"));
}

function vendors() {
    return src([
        "node_modules/jquery/dist/jquery.js",
        "node_modules/parallax-js/dist/parallax.min.js",
        "src/js/libs/jquery-ui.min.js",
    ])
        .pipe(concat("vendor.min.js"))
        .pipe(uglify())
        .pipe(dest("src/js"));
}

function scripts() {
    return src(["src/js/tabs.js"])
        .pipe(concat("scripts.min.js"))
        .pipe(uglify())
        .pipe(dest("src/js"))
        .pipe(browserSync.stream());
}

function styles() {
    return src("src/scss/styles.scss")
        .pipe(scss({ outputStyle: "compressed" }))
        .pipe(concat("styles.min.css"))
        .pipe(
            autoprefixer({
                overrideBrowserslist: ["last 10 version"],
                grid: true,
            })
        )
        .pipe(dest("src/css"))
        .pipe(browserSync.stream());
}

function build() {
    return src(
        [
            "src/css/styles.min.css",
            // "src/fonts/**/*",
            "src/js/scripts.min.js",
            "src/js/vendor.min.js",
            "src/*.html",
        ],
        { base: "src" }
    ).pipe(dest("dist"));
}

function watching() {
    watch(["src/scss/**/*.scss"], styles);
    watch(["src/js/tabs.js"], scripts);
    watch(["src/*.html"]).on("change", browserSync.reload);
}

exports.styles = styles;
exports.watching = watching;
exports.browsersync = browsersync;
exports.scripts = scripts;
exports.vendors = vendors;
exports.images = images;
exports.cleanDist = cleanDist;
exports.cleanSrc = cleanSrc;

exports.build = series(
    styles,
    scripts,
    vendors,
    cleanDist,
    images,
    build,
    cleanSrc
);
exports.default = parallel(
    cleanSrc,
    styles,
    scripts,
    vendors,
    browsersync,
    watching
);
