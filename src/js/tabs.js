(function ($) {
    "use strict";

    /**
     * Табы
     */
    $.fn.tabs = function () {
        var $self = $(this);
        var $tabHeaders = $self
            .find(".js-tab-header")
            .filter(function (index, el) {
                return $(el).parentsUntil($self).length === 1;
            });
        var $tabContent = $self
            .find(".js-tab-content")
            .filter(function (index, el) {
                return $(el).parentsUntil($self).length === 1;
            });

        /**
         * Активация таба по его индексу
         * @param {Number} index - индекс таба, который нужно активировать
         */
        var selectTab = function (index) {
            $tabHeaders.removeClass("active").eq(index).addClass("active");
            $tabContent.removeClass("active").eq(index).addClass("active");
        };

        /**
         * Инициализаиця
         */
        var init = function () {
            selectTab(0);

            // Обработка событий
            $tabHeaders.on("click", function () {
                selectTab($(this).index());
            });
        };

        init();

        this.selectTab = selectTab;

        return this;
    };

    // Инициализируем табы на всех блоках с классом 'js-tabs'
    $(".js-tabs").each(function () {
        $(this).data("tabs", $(this).tabs());
    });

    // Sticky header

    const headerTop = $(".header").offset().top;

    $(window).scroll(function () {
        if ($(window).scrollTop() > headerTop) {
            $(".header").addClass("sticky");
        } else {
            $(".header").removeClass("sticky");
        }
    });

    // Select

    $(".tabs-select").on("change", function () {
        var id = $(".tabs-select option:selected").attr("data-tab"),
            content = $(`.js-tab-content[data-tab="${id}"]`);

        $(".js-tab-content.active").removeClass("active");
        content.addClass("active");
    });


    // Draggable image

    $("#draggable").draggable({
        refreshPositions: true,
    });


    // Parallax image

    const scene = document.getElementById("scene");
    const parallaxInstance = new Parallax(scene, {
        hoverOnly: true
    });

    parallaxInstance.friction(0.2, 0.2);
    
})(jQuery);
